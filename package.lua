  return {
    name = "SoniEx2/http-cookies-required",
    version = "0.0.2",
    description = "HTTP 402 Cookies Required",
    tags = { "http", "cookies" },
    license = "MIT",
    author = { name = "Soni L.", email = "fakedme@gmail.com" },
    homepage = "https://bitbucket.org/SoniEx2/http-cookies-required",
    dependencies = {},
    files = {
      "**.lua",
      "!test*"
    }
  }
  
